package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.component.Bootstrap;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.SessionRepository;
import ru.tsc.kyurinova.tm.repository.UserRepository;

import java.sql.SQLException;

public class SessionServiceTest {

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private Session session;

    @NotNull
    private String userId;

    @NotNull
    private final IUserService userService = new UserService(new ConnectionService(new PropertyService()), new LogService(), new PropertyService());


    public SessionServiceTest() {
        sessionService = new SessionService(
                new ConnectionService(new PropertyService()), new LogService(), new Bootstrap()
        );
    }

    @Before
    public void before() throws SQLException {
        @NotNull final String userLogin = "userLogin";
        @NotNull final String userPassword = "userPassword";
        userId = userService.create(userLogin, userPassword).getId();
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void openTest() throws SQLException {
        @NotNull final String newUserId = userService.create("test", "test").getId();
        final int initialSize = sessionService.getSize();
        @NotNull final Session newSession = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(newSession.getSignature());
        sessionService.close(newSession);
        userService.removeById(newUserId);

    }

    @Test
    public void closeTest() throws SQLException {
        final int initialSize = sessionService.getSize();
        @NotNull final Session session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    public void validateTest() throws SQLException {
        @NotNull final String newUserId = userService.create("admin", "admin").getId();
        @NotNull final Session newSession = sessionService.open("admin", "admin");
        sessionService.validate(newSession);
        sessionService.close(newSession);
        userService.removeById(newUserId);
    }

    @Test
    public void validateRoleTest() throws SQLException {
        @NotNull final String newUserId = userService.create("admin", "admin", Role.ADMIN).getId();
        @NotNull final Session newSession = sessionService.open("admin", "admin");
        sessionService.validate(newSession, Role.ADMIN);
        sessionService.close(newSession);
        userService.removeById(newUserId);
    }

    @After
    public void after() throws SQLException {
        sessionService.close(session);
        userService.removeById(userId);
    }

}
