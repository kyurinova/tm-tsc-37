package ru.tsc.kyurinova.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.*;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.endpoint.*;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.repository.*;
import ru.tsc.kyurinova.tm.service.*;
import ru.tsc.kyurinova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Setter
@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, logService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService, logService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, logService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, logService, this);

    @NotNull
    private final IAdminDataService adminDataService = new AdminDataService(this);

    @NotNull
    private final IUserService userService = new UserService(connectionService, logService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this, adminDataService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(sessionService, projectTaskService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(userService, sessionService);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(userService, sessionService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(adminDataService, sessionService);


    public void start(@Nullable final String[] args) {
        try {
            System.out.println("** WELCOME TO TASK MANAGER **");
            initPID();
            initEndpoint();
            backup.init();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(sessionEndpoint);
        registry(adminDataEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
