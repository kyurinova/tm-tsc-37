package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IService;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public AbstractService(@NotNull final IConnectionService connectionService, @NotNull final ILogService logService) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public void remove(@Nullable final E entity) throws SQLException {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.remove(entity);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll() throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findAll();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(final Comparator<E> comparator) throws SQLException {
        if (comparator == null) return Collections.emptyList();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findAll(comparator);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.clear();
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findById(id);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final Integer index) throws SQLException {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findByIndex(index);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.removeById(id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) throws SQLException {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.removeByIndex(index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) return false;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.existsById(id);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.existsByIndex(index);
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public int getSize() throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.getSize();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

}
