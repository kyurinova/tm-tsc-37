package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IRepository;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.model.AbstractEntity;
import ru.tsc.kyurinova.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract E fetch(@NotNull final ResultSet row) throws SQLException;

    @Override
    public void remove(@NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<E> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull Comparator comparator) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " ORDER BY ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Sort.valueOf(comparator.toString()).name());
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new EntityNotFoundException();
        return result;
    }


    @Override
    public void clear() throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }


    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final String query = "SELECT * FROM  " + getTableName() + " LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    public void add(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getRole().toString());
        statement.setString(9, user.getLocked());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();

    }

    @Override
    public void removeByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final String id = findByIndex(index).getId();
        removeById(id);
    }

    @Override
    public boolean existsById(@NotNull final String id) throws SQLException {
        try {
            @Nullable final E entity = findById(id);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) throws SQLException {
        @Nullable final E entity = findByIndex(index);
        return entity != null;
    }

    @Override
    public int getSize() throws SQLException {
        @NotNull final String query = "SELECT count(*) AS count FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt("count");
    }

}
