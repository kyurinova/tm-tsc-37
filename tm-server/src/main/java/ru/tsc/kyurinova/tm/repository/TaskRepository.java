package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    private final static String TASK_TABLE = "task";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TASK_TABLE;
    }

    @NotNull
    @Override
    protected Task fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Task task = new Task();
        task.setId(row.getString("row_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("descr"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        task.setStartDate(row.getTimestamp("start_dt"));
        task.setFinishDate(row.getTimestamp("finish_dt"));
        task.setProjectId(row.getString("project_id"));
        return task;
    }

    @Override
    public void add(@NotNull String userId, @NotNull Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (row_id, name, descr, user_id, status, created, start_dt, finish_dt, project_id)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getUserId());
        statement.setString(5, task.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
        @Nullable final Date startDate = task.getStartDate();
        @Nullable final Date finishDate = task.getFinishDate();
        statement.setTimestamp(7, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setTimestamp(8, finishDate == null ? null : new Timestamp(finishDate.getTime()));
        statement.setString(9, task.getProjectId() == "" ? null : task.getProjectId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(@NotNull List<Task> tasks) throws SQLException {
        for (Task task : tasks) {
            add(task.getUserId(), task);
        }
    }

    @NotNull
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start_dt = ? WHERE row_id = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, id);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start_dt = ? WHERE row_id in (SELECT row_id from  " + getTableName() + " where  user_id = ? LIMIT 1 OFFSET ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setInt(4, index);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start_dt = ? WHERE name = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, name);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, finish_dt = ? WHERE row_id = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, id);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, finish_dt = ? WHERE row_id in (SELECT row_id from  " + getTableName() + " where  user_id = ? LIMIT 1 OFFSET ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setInt(4, index);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, finish_dt = ? WHERE name = ? AND user_id = ? AND status <> ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status);
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, name);
        statement.setString(4, userId);
        statement.setString(5, status);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, descr = ?" +
                " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) throws SQLException {
        @NotNull final String projectId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, descr = ?" +
                " WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws SQLException {
        if (Status.IN_PROGRESS.toString().equals(status))
            startById(userId, id);
        else if (Status.COMPLETED.toString().equals(status))
            finishById(userId, id);
        else {
            @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND row_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setString(3, id);
            statement.executeUpdate();
            statement.close();
        }
    }

    @Override
    public void changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) throws SQLException {
        if (Status.IN_PROGRESS.toString().equals(status))
            startByIndex(userId, index);
        else if (Status.COMPLETED.toString().equals(status))
            finishByIndex(userId, index);
        else {
            @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE row_id in (SELECT row_id from  " + getTableName() + " where  user_id = ? LIMIT 1 OFFSET ?)";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setInt(3, index);
            statement.executeUpdate();
            statement.close();
        }
    }

    @Override
    public void changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) throws SQLException {
        if (Status.IN_PROGRESS.toString().equals(status))
            startByName(userId, name);
        else if (Status.COMPLETED.toString().equals(status))
            finishByName(userId, name);
        else {
            @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND name = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setString(3, name);
            statement.executeUpdate();
            statement.close();
        }
    }

    @Override
    public void bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET project_id = ? WHERE user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void unbindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ? WHERE project_id = ? AND user_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, Types.VARCHAR);
        statement.setString(2, projectId);
        statement.setString(3, userId);
        statement.setString(4, taskId);
        statement.executeUpdate();
        statement.close();
    }


    @Nullable
    @Override
    public Task findByProjectAndTaskId(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND project_id = ? AND row_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.setString(3, taskId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

}
