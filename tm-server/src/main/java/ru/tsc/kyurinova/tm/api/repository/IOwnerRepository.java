package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.AbstractOwnerEntity;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    void remove(@NotNull String userId, @NotNull E entity) throws SQLException;

    @NotNull
    List<E> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull String comparator) throws SQLException;

    void clear(@NotNull String userId) throws SQLException;

    @Nullable
    E findById(@NotNull String userId, @NotNull String id) throws SQLException;

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    void removeById(@NotNull String userId, @NotNull String id) throws SQLException;

    void removeByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    boolean existsById(@NotNull String userId, @NotNull String id) throws SQLException;

    boolean existsByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

}