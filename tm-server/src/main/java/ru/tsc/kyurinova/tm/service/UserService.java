package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.UserRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService, logService);
        this.propertyService = propertyService;
    }

    @NotNull
    protected IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @Override
    public void addAll(@NotNull final List<User> users) throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.addAll(users);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }


    @Nullable
    @Override
    public User findByLogin(@Nullable String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        @Nullable User user;
        try {
            user = userRepository.findByLogin(login);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        @Nullable User user;
        try {
            user = userRepository.findByEmail(email);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    public void isLoginExists(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.isLoginExists(login);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void isEmailExists(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.isEmailExists(email);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.removeByLogin(login);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        isLoginExists(login);
        isEmailExists(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new EmptyUserIdException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.setPassword(userId, password);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.updateUser(userId, firstName, lastName, middleName);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("Y");
        try {
            userRepository.setLockedByLogin(login, "Y");
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("N");
        try {
            userRepository.setLockedByLogin(login, "N");
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return user;
    }
}
