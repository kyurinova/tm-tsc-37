package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findByEmail(@Nullable String email) throws SQLException;

    void isLoginExists(@Nullable String login) throws SQLException;

    void isEmailExists(@Nullable String email) throws SQLException;

    void addAll(@NotNull List<User> users) throws SQLException;

    @Nullable
    User findByLogin(@Nullable String login) throws SQLException;

    void removeByLogin(@Nullable String login) throws SQLException;

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws SQLException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws SQLException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws SQLException;

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password) throws SQLException;

    @Nullable
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws SQLException;

    @Nullable
    User lockUserByLogin(@Nullable String login) throws SQLException;

    @Nullable
    User unlockUserByLogin(@Nullable String login) throws SQLException;

}
