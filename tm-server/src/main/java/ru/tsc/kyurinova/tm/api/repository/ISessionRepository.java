package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.model.Session;

import java.sql.SQLException;

public interface ISessionRepository extends IRepository<Session> {

    void add(@NotNull Session session) throws SQLException;

    @NotNull
    public boolean exists(@NotNull final String id) throws SQLException;

}
