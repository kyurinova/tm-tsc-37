package ru.tsc.kyurinova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.SessionRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(connectionService, logService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    @NotNull
    public Session open(@NotNull final String login, @NotNull final String password) throws SQLException {
        final boolean check = checkDataAccess(login, password);
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        try {
            sessionRepository.add(session);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }
        return sign(session);


    }

    @Override
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) throws SQLException {
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session sign(@NotNull final Session session) throws SQLException {
        session.setSignature(null);
        @NotNull final String salt = serviceLocator.getPropertyService().getSessionSecret();
        final int cycle = serviceLocator.getPropertyService().getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) throws SQLException {
        if (session == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        try {
            sessionRepository.remove(session);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }

    }

    @Override
    public void validate(@NotNull final Session session) throws SQLException {
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final Session sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        try {
            if (!sessionRepository.exists(session.getId())) throw new AccessDeniedException();
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DBException();
        } finally {
            connection.close();
        }

    }

    @Override
    public void validate(@NotNull Session session, @NotNull Role role) throws SQLException {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final User user = serviceLocator.getUserService().findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

}
