-- Table: public.project

-- DROP TABLE IF EXISTS public.project;

CREATE TABLE IF NOT EXISTS public.project
(
    row_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    descr character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED'::character varying,
    created timestamp without time zone NOT NULL,
    start_dt timestamp without time zone,
    finish_dt timestamp without time zone,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT project_pkey PRIMARY KEY (row_id),
    CONSTRAINT project_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (row_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.project
    OWNER to postgres;

COMMENT ON TABLE public.project
    IS 'Projects';

CREATE TABLE IF NOT EXISTS public.session
(
    row_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    signature character varying(255) COLLATE pg_catalog."default",
    time_stamp timestamp without time zone,
    CONSTRAINT session_pkey PRIMARY KEY (row_id),
    CONSTRAINT session_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (row_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.session
    OWNER to postgres;

COMMENT ON TABLE public.session
    IS 'Sessions';
	

CREATE TABLE IF NOT EXISTS public.task
(
    row_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    project_id character varying(255) COLLATE pg_catalog."default",
    status character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED'::character varying,
    created timestamp without time zone NOT NULL,
    start_dt timestamp without time zone,
    finish_dt timestamp without time zone,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    descr character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT task_pkey PRIMARY KEY (row_id),
    CONSTRAINT task_fkey1 FOREIGN KEY (user_id)
        REFERENCES public.users (row_id) MATCH FULL
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT task_fkey2 FOREIGN KEY (project_id)
        REFERENCES public.project (row_id) MATCH FULL
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.task
    OWNER to postgres;

COMMENT ON TABLE public.task
    IS 'Tasks';
	
CREATE TABLE IF NOT EXISTS public.users
(
    row_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    login character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password_hash character varying(255) COLLATE pg_catalog."default" NOT NULL,
    role character varying(255) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default",
    fst_name character varying(255) COLLATE pg_catalog."default",
    last_name character varying(255) COLLATE pg_catalog."default",
    mid_name character varying(255) COLLATE pg_catalog."default",
    locked character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT user_pkey PRIMARY KEY (row_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.users
    OWNER to postgres;

COMMENT ON TABLE public.users
    IS 'Users';	
	