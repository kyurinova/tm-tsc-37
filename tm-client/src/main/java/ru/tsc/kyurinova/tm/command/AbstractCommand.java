package ru.tsc.kyurinova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.enumerated.Role;

public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    @Override
    public String toString() {
        return super.toString();
    }
}
